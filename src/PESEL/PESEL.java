package PESEL;

import java.util.Objects;
/* PESEL VALIDATION
 *
 * PESEL is a unique identification number for every Polish citizen.
 * It consists of 11 digits, where the first 6 digits are the date of birth
 * in the format YYMMDD, the next 5 digits are the serial number of the
 * person and the last digit is a checksum.
 *
 * To be sure that PESEL is correct we need to check if days and months are of acceptable values
 *
 * and if the checksum is correct.
 *
 * The checksum is calculated as follows:
 * 1. Multiply each digit by a number from the following list:
 * 1, 3, 7, 9, 1, 3, 7, 9, 1, 3
 * 2. Add the results of the multiplication
 * 3. Return the remainder of the division by 10 (sum % 10)
 * 4. Verify if the checksum is equal to the last digit of the PESEL
 *
 */

public class PESEL {
    static String PESEL;
    //Param constructor
    public PESEL(String str){
        this.PESEL = str;
    }

    //compares two PESEL numbers (PESEL is represented as a String)
    public static boolean compare(String s1, String s2){
        return s1.equals(s2);
    }


    //getting birthDay birthMonth and birthYear, and checking if they are correct
    private static boolean leapYear(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }
    public static int getBirthYear(){
        int year = Integer.parseInt(PESEL.substring(0,2));
        int month = Integer.parseInt(PESEL.substring(2,4));
        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }
    public static int getBirthMonth(){
        int month = Integer.parseInt(PESEL.substring(2,4));
        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }
    private static boolean isCorrectMonth(){
        int month = getBirthMonth();
        return month > 0 && month < 13;
    }
    private static boolean birthDay(){
        int day = Integer.parseInt(PESEL.substring(4,6));
        int month = getBirthMonth();
        int year = getBirthYear();
        if ((day >0 && day < 32) &&
                (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)) {
            return true;
        }
        else if ((day > 0 && day < 31) &&
                (month == 4 || month == 6 || month == 9 || month == 11)) {
            return true;
        }
        //checking if birthday is valid of not based on leap year logic
        else if ((day > 0 && day < 30 && leapYear(year)) ||
                (day > 0 && day < 29 && !leapYear(year))) {
            return true;
        }
        else {
            return false;
        }
    }
    private static boolean checkSum(){
        int[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            if (PESEL != null) {
                sum += weights[i] * Integer.parseInt(PESEL.substring(i, i + 1));
            }
        }
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;
        if (PESEL != null) {
            return sum == Integer.parseInt(PESEL.substring(10));
        }
        return false;
    }

    public static boolean check(String PESEL){
        if (PESEL.length() != 11){
            return false;
        }
        else return checkSum() && isCorrectMonth() && birthDay();
    }
    public int hashCode() {
        return Objects.hash(PESEL);
    }


}
