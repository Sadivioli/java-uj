package PESEL;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

public class PeselValidator {

    private static final Logger LOGGER = Logger.getLogger(PeselValidator.class.getName());

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(new File("src/PESEL/pesel.txt"));
            FileWriter fileWriter = new FileWriter("src/PESEL/validPesel.txt");


            while (scanner.hasNextLine()) {
                String pesel = scanner.nextLine();
                if (PESEL.check(pesel)) {
                    fileWriter.write(pesel + " PESEL is valid \n");
                } else {
                    fileWriter.write(pesel + " PESEL is invalid \n");
                }
            }

            scanner.close();
            fileWriter.close();

        } catch (FileNotFoundException e) {
            LOGGER.info("File not found");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
