package reader;

import java.io.*;

public class Reader {
    public static String getString(){
        String text = null;
        try {
            InputStreamReader rd = new InputStreamReader(System.in);
            BufferedReader bfr = new BufferedReader(rd);

            text = bfr.readLine();
        }catch (IOException e){e.printStackTrace();}
        return text;
    }
}
